package game

var state = arrayOf(
    arrayOf(' ', '1', '2', '3'),
    arrayOf('1', '-', '-', '-'),
    arrayOf('2', '-', '-', '-'),
    arrayOf('3', '-', '-', '-')
)

var currentPlayer = 'X'
var setRow: Int = 0
var setCol: Int = 0
var loop = true
var round = 0

fun main() {
    showWelcome()
    while(loop){
        showTable(state)
        showTurn()
        setInput(readLine()!!)
        setTable(setRow, setCol, currentPlayer)
    }
}

fun setTable(row:Int, col:Int, player:Char){
    try {
        if(state[row][col] == '-'){
            state[row][col] = player
            round++
            checkHorizontal(currentPlayer)
            checkVertical(currentPlayer)
            checkDiagonal(currentPlayer)
            checkIsDraw(round)
            changePlayer(player)
        }
    }catch (e: Throwable){
        println("Please Input correct values!")

    }
}

fun changePlayer(player: Char) {
    if(player == 'X'){
        currentPlayer = 'O'
    }
    else if(player == 'O'){
        currentPlayer = 'X'
    }
}

fun setInput(input:String){
    try {
        setRow = 0
        setCol = 0
        val splitInput = input.split(" ")
        setRow = splitInput[0].toInt()
        setCol = splitInput[1].toInt()
    }catch (e: Throwable){
        println("Please Input correct values!")
    }
}


val showTurn : () -> Unit = {
    if(currentPlayer == 'X'){
        print("Turn $currentPlayer Input row col: ")
    }else if(currentPlayer == 'O'){
        print("Turn $currentPlayer Input row col: ")
    }

}

val showTable : (Array<Array<Char>>) -> Unit = {state ->
    for( row in state ){
        for( col in row ){
            print("$col ")
        }
        println()
    }
}

val showWelcome : () -> Unit = {
    println("Welcome to Tic-Tac-Toe.")
}


fun checkIsDraw(round: Int) {
    var count = round
    for (i in state.indices) {
        for (j in state[i].indices) {
            if (state[i][j] == '-') {
                count--
            }
        }
    }
    if (count == round) {
        println("DRAW!!!!")
        loop = false
    }
}


fun checkDiagonal(player: Char) {
    if (state[1][1] == player && state[2][2] == player && state[3][3] == player) {
        println("$player WIN! ")
        loop = false
    } else if (state[1][3] == player && state[2][2] == player && state[3][1] == player) {
        println("$player WIN! ")
        loop = false
    }
}


fun checkVertical(player: Char) {
    var count = 0
    for (i in state.indices) {
        for (j in state[i].indices) {
            if (state[j][i] == player) {
                count++
            }
        }
        if (count == 3) {
            loop = false
            println("$player WIN! ")
        }
        count = 0
    }
}

fun checkHorizontal(player: Char) {
    var count = 0
    for (i in state.indices) {
        for (j in state[i].indices) {
            if (state[i][j] == player) {
                count++
            }
        }
        if (count == 3) {
            println("$player WIN! ")
            loop = false
        }
        count = 0
    }
}
